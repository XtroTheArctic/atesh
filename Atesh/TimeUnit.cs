﻿namespace Atesh;

public enum TimeUnit
{
    Millisecond,
    Second,
    Minute,
    Hour,
    Day,
    Month,
    Year
}